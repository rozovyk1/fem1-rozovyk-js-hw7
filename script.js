function appendList(arr) {

    let list = document.createElement('ul');

    let elements = arr.map(function(elem) {
        // Creating list item
        let listItem = document.createElement('li');
        // Adding list item into the list
        list.appendChild(listItem);
        // Cheking if element is an array
        if (Array.isArray(elem)) {
            listItem.appendChild(appendList(elem));
        } else {
            // Setting element into the list item
            listItem.innerHTML = `${elem}`;
        }

        return listItem;
    });

    return list;
}


let arr1 = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
document.body.appendChild(appendList(arr1));

let arr2 = ['1', '2', '3', 'sea', 'user', 23];
document.body.appendChild(appendList(arr2));

let arr3 = ['sdfw', 'wefwefwef', ['sea', 'Kharkiv', ['sea', 'Kharkiv', 'Odessa'], 'Odessa'], 'user', ['ololo', 'trololo', 'blahblahblah'], 23];
document.body.appendChild(appendList(arr3));


